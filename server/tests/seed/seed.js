const { ObjectId } = require('mongodb');
const jwt = require('jsonwebtoken');
const { Todo } = require('./../../models/todo');
const { User } = require('./../../models/user');

const userOneId = new ObjectId();
const userTwoId = new ObjectId();
const users = [{
   _id: userOneId,
   email: 'test@test.com',
   password: 'password1',
   tokens: [{
      access: 'auth',
      token: jwt.sign({ _id: userOneId, access: 'auth' }, 'abc123').toString()
   }]
}, {
   _id: userTwoId,
   email: 'test2@test.com',
   password: 'password2'
}]

const todos = [{
   _id: new ObjectId(),
   text: "First test todo"
}, {
   _id: new ObjectId(),
   text: "test 2",
   completed: true,
   completedAt: 333
}];

const populateTodos = (done) => {
   Todo.remove({}).then(() => {
      return Todo.insertMany(todos);
   }).then(() => done());
};

const populateUsers = (done) => {
   User.remove({}).then(() => {
      var user1 = new User(users[0]).save();
      var user2 = new User(users[1]).save();

      return Promise.all([user1, user2]);
   }).then(() => done());
};

module.exports = { todos, populateTodos, users, populateUsers };